{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module VDOM.PatchTest where

import           Control.Lens         hiding (Index)
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Hashable
import           Data.HashMap.Strict  (HashMap)
import qualified Data.HashMap.Strict  as HashMap
import           Data.Maybe           (fromMaybe)
import           Data.Text            (Text)
import qualified Data.Text            as Text
import           Data.Vector          (Vector)
import qualified Data.Vector          as Vector
import           GHC.Generics
import           Hedgehog
import qualified Hedgehog.Gen         as Gen
import qualified Hedgehog.Range       as Range

import           VDOM


newtype TestNode = TestNode { nodeId :: Int }
  deriving (Eq, Show, Generic, Hashable)

data TestOperation
  = OpCreate (Maybe Text)
  | OpAppendChild TestNode TestNode
  | OpReplaceIn TestNode TestNode TestNode
  | OpDelete TestNode TestNode
  | OpSetContents Text TestNode
  deriving (Eq, Show)

makePrisms ''TestOperation

data DomState = DomState { _currentId :: Int, _nodeChildren :: HashMap TestNode (Vector TestNode) }

makeLenses ''DomState

initialDomState :: DomState
initialDomState = DomState 0 mempty

newtype TestDom a = TestDom { unTestDom :: StateT DomState (Writer (Vector TestOperation)) a }
  deriving (Functor, Applicative, Monad, MonadState DomState, MonadWriter (Vector TestOperation))

instance MonadDom TestDom where
  type Node TestDom = TestNode
  create contents' = do
    tell [OpCreate contents']
    currentId += 1
    node <- uses currentId TestNode
    nodeChildren %= HashMap.insert node mempty
    pure node
  appendChild parent node = do
    nodeChildren %= HashMap.adjust (`Vector.snoc` node) parent
    tell [OpAppendChild parent node]
  replaceIn parent oldNode newNode = do
    let replaceOld = Vector.map $ \n -> if n == oldNode then newNode else n
        removeNew = Vector.filter (/= newNode)
    nodeChildren %= HashMap.adjust (replaceOld . removeNew) parent
    tell [OpReplaceIn parent oldNode newNode]
  deleteFrom parent child = do
    nodeChildren %= HashMap.adjust (Vector.filter (/= child)) parent
    tell [OpDelete parent child]
  setContents content node =
    -- We don't actually do anything with the content.
    tell [OpSetContents content node]
  getChildren node =
    fromMaybe mempty <$> uses nodeChildren (HashMap.lookup node)


runTestDom :: DomState -> TestDom a -> (a, DomState, Vector TestOperation)
runTestDom initial (TestDom ma) =
  let ((a, s), instr) = runWriter (runStateT ma initial)
  in  (a, s, instr)

runPatch :: MonadTest m => VNode -> VNode -> m (Vector TestOperation)
runPatch first second = do
  let (node, state', _    ) = runTestDom initialDomState (createNode first)
      (_   , _     , instr) = runTestDom state' $ case patch first second of
        Replace m -> void m
        Modify  m -> m node
        Keep      -> pure ()
  footnoteShow instr
  pure instr

-- Generators

genVParent :: MonadGen m => Range Int -> m VNode -> m VNode
genVParent r genChild =
  VParent . Vector.map (Keyed Nothing) . Vector.fromList <$> Gen.list r genChild

genVLeaf :: MonadGen m => Range Int -> m VNode
genVLeaf r = VLeaf . Text.pack <$> Gen.string r Gen.unicode

genVNode :: MonadGen m => Range Int -> m VNode
genVNode r = Gen.frequency
  [(1, genVParent r (Gen.scale (div 2) (genVNode r))), (5, genVLeaf r)]

-- Helpers

modifyChildren
  :: (Vector (Keyed VNode) -> Vector (Keyed VNode)) -> VNode -> VNode
modifyChildren f (VParent children') = VParent (f children')
modifyChildren _ vnode               = vnode

append :: VNode -> VNode -> VNode
append x = modifyChildren $ \xs -> Vector.snoc xs (Keyed Nothing x)

prepend :: VNode -> VNode -> VNode
prepend x = modifyChildren $ Vector.cons (Keyed Nothing x)

numberOf ctor = lengthOf (traverse . ctor)

-- Properties

hprop_equal_nodes_need_no_update :: Property
hprop_equal_nodes_need_no_update = property $ do
  -- Patch a with the exact same VDOM node, expecting no DOM
  -- operations.
  vnode <- forAll $ genVNode (Range.linear 0 10)
  operations <- runPatch vnode vnode
  operations === []

hprop_appended_nodes_creates_new_dom_nodes :: Property
hprop_appended_nodes_creates_new_dom_nodes = property $ do
  -- Generate a VParent and an extra node to prepend.
  vnode <- forAll
    $ genVParent (Range.linear 0 10) (genVNode (Range.linear 0 10))
  extra <- forAll $ genVNode (Range.linear 0 10)
  -- Run the patching.
  operations <- runPatch vnode (append extra vnode)
  -- The patch should only create and append DOM nodes for as many new
  -- VDOM nodes we have.
  let extraNodes = length (universeOf vnodeChildren extra)
  numberOf _OpCreate operations === extraNodes
  numberOf _OpAppendChild operations === extraNodes
  -- There should be no replacements or deletions.
  numberOf _OpReplaceIn operations === 0
  numberOf _OpDelete operations === 0

-- NOTE: This currently fails!
hprop_prepended_node_does_not_cause_full_rebuild :: Property
hprop_prepended_node_does_not_cause_full_rebuild = property $ do
  -- Generate a VParent and an extra node to prepend.
  vnode <- forAll
    $ genVParent (Range.linear 0 10) (genVNode (Range.linear 0 10))
  extra <- forAll $ genVNode (Range.linear 0 10)
  -- Run the patching.
  operations <- runPatch vnode (prepend extra vnode)
  -- The patch should only create and append DOM nodes for as many new
  -- VDOM nodes we have.
  let extraNodes = length (universeOf vnodeChildren extra)
  numberOf _OpCreate operations === extraNodes
  numberOf _OpAppendChild operations === extraNodes
  -- There should be no replacements or deletions.
  numberOf _OpReplaceIn operations === 0
  numberOf _OpDelete operations === 0
