{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFunctor      #-}
{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeFamilies       #-}
module VDOM where

import           Control.Lens
import           Data.Data
import           Data.Data.Lens
import           Data.Foldable
import           Data.Functor
import           Data.Text      (Text)
import           Data.Vector    (Vector)
import qualified Data.Vector    as Vector
import           Debug.Trace

data Keyed a = Keyed { key :: Maybe Int, unKeyed :: a }
  deriving (Eq, Show, Data, Functor)

instance Data a => Plated (Keyed a) where
  plate = uniplate

type Index = Int

data VNode
  = VLeaf { _contents :: Text }
  | VParent { _children :: Vector (Keyed VNode) }
  deriving (Eq, Show, Data)

makePrisms ''VNode

instance Plated VNode where
  plate = uniplate

-- TODO: Can this be derived automatically, or made by composing
-- existing lenses and prisms?
vnodeChildren :: Traversal' VNode VNode
vnodeChildren _ n@VLeaf {}   = pure n
vnodeChildren f (VParent cs) =
  VParent <$> traverse (\(Keyed i x) -> Keyed i <$> f x) cs

class Monad m => MonadDom m where
  type Node m
  create :: Maybe Text -> m (Node m)
  appendChild :: Node m -> Node m -> m ()
  replaceIn :: Node m -> Node m -> Node m -> m ()
  deleteFrom :: Node m -> Node m -> m ()
  setContents :: Text -> Node m -> m ()
  getChildren :: Node m -> m (Vector (Node m))

createNode :: MonadDom m => VNode -> m (Node m)
createNode (VLeaf c) = create (Just c)
createNode (VParent children') = do
  node <- create Nothing
  forOf_ each children' $ \child ->
    createNode (unKeyed child) >>= appendChild node
  pure node

data Patch m
  = Replace (m (Node m))
  | Modify (Node m -> m ())
  | Keep

patch :: MonadDom m => VNode -> VNode -> Patch m
patch = go
  where
    go (VLeaf c1) (VLeaf c2) | c1 == c2  = Keep
                             | otherwise = Modify (setContents c2)
    go (VParent c1) (VParent c2) = patchChildren c1 c2
    go _            vnode        = Replace (createNode vnode)

-- Naive implementation, can't handle reordering well.
patchChildren
  :: MonadDom m
  => Vector (Keyed VNode)
  -> Vector (Keyed VNode)
  -> Patch m
patchChildren oldVnodes newVnodes
  | Vector.null oldVnodes && Vector.null newVnodes = Keep
  | otherwise = Modify $ \parent -> do
    children' <- getChildren parent
    let maxLength =
          maximum [length children', length oldVnodes, length newVnodes]
        indices' :: Vector Int
        indices' = Vector.enumFromN 0 (fromIntegral maxLength)
    traverse_
      (go parent)
      (Vector.zip4 indices'
                   (padMaybes maxLength oldVnodes)
                   (padMaybes maxLength newVnodes)
                   (padMaybes maxLength children')
      )
  where
    go parent' = \case
      (_i, Just (Keyed _ child1), Just (Keyed _ child2), Just node) ->
        case patch child1 child2 of
          Replace makeNew -> replaceIn parent' node =<< makeNew
          Modify  modify  -> modify node
          Keep            -> pure ()
      (_i, _, Nothing, Just node) ->
        deleteFrom parent' node
      (_i, Nothing, Just (Keyed _ child), Nothing) ->
        appendChild parent' =<< createNode child
      (i, child1, child2, node) -> traceShowM (i, child1, child2, node $> "node")

padMaybes :: Int -> Vector a -> Vector (Maybe a)
padMaybes len xs = Vector.generate len (xs Vector.!?)
